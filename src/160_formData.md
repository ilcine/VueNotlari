# formData

## Ex:1 

link
```
<a href="#"  @click="submitPost('pdf')" >PDF Export </a>
```
form
```
<form method="post" @submit.prevent="submitPost()">
  <input type="hidden" name="_token" value="{!! csrf_token() !!}">
  
  <select   id="ExportTipi" name="ExportTipi">
    <option selected disabled value="1" >Choose Export</option>
    <option value="xls">xls Excel</option>
    <option value="pdf">pdf Export</option>
  </select> 
 <button type="submit" name="button">Submit</button>
</form>
```

script
```
methods: {
  submitPost(e) {
    let app = this; 
    let formData = new FormData();
    formData.append('id', 1);
    formData.append('type', e);
    
   # entry test 
   for (let [key, value] of formData.entries()) {
    console.log(key, ':', value);
   };
 }
} 
``` 

## ex2 

```
<button @click="paraKoy(selectedMoney)"> select </button>
	
	<select  v-model="selectedMoney" >
		<option v-for="optionMoney in optionsMoney" v-bind:value="optionMoney.selectValue"  >
				{{ optionMoney.selectText }} 
		</option> 
	</select>
```

```
data()
	{    
		return {	
			selectedMoney: 10,
			optionsMoney: [
					{ selectText: '1 ', selectValue:1},
					{ selectText: '5 ', selectValue:5},
					{	selectText: '10 ', selectValue:10},
					{	selectText: '20 ', selectValue:20}
			], 
	    }		
	}
```	
	